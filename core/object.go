// Package core contains the most basic structures used within screeptists
package core

import (
	"errors"
	"fmt"
	"strings"
)

// TypeName returns a string representation of a type
// or '?' if type wasn't recognized
func (obj *Object) TypeName() string {
	var typeN int8 = obj.valueType
	switch typeN {
	case 0:
		return "void"
	case 1:
		return "string"
	case 2:
		return "int"
	case 3:
		return "float"
	case 4:
		return "bool"
	default:
		return "?"
	}
}

// Object is a dynamically typed value (void, string, int, float and bool)
type Object struct {
	valueType   int8
	StringValue string
	IntValue    int32
	FloatValue  float32
	BoolValue   bool
}

// NewObject returns an Object pointer to a void Object
func NewObject() *Object {
	var tmp *Object = new(Object)
	tmp.Flush()
	return tmp
}

// Flush resets all the values and sets the type to void
func (obj *Object) Flush() {
	obj.valueType = 0
	obj.StringValue = ""
	obj.IntValue = 0
	obj.FloatValue = 0.0
	obj.BoolValue = false
}

// IsVoid returns true if Object stores nothing
func (obj *Object) IsVoid() bool {
	return obj.valueType == 0
}

// IsString returns true if Object stores a string
func (obj *Object) IsString() bool {
	return obj.valueType == 1
}

// IsInt returns true if Object stores an integer
func (obj *Object) IsInt() bool {
	return obj.valueType == 2
}

// IsFloat returns true if Object stores a float
func (obj *Object) IsFloat() bool {
	return obj.valueType == 3
}

// IsBool returns true if Object stores a bool
func (obj *Object) IsBool() bool {
	return obj.valueType == 4
}

// SetString flushes the Object and stores a string in it
func (obj *Object) SetString(val string) {
	obj.Flush()
	obj.valueType = 1
	obj.StringValue = val
}

// SetInt flushes the Object and stores an integer in it
func (obj *Object) SetInt(val int32) {
	obj.Flush()
	obj.valueType = 2
	obj.IntValue = val
}

// SetFloat flushes the Object and stores a float in it
func (obj *Object) SetFloat(val float32) {
	obj.Flush()
	obj.valueType = 3
	obj.FloatValue = val
}

// SetBool flushes the Object and stores a bool in it
func (obj *Object) SetBool(val bool) {
	obj.Flush()
	obj.valueType = 4
	obj.BoolValue = val
}

// Str2Object parses a string and returns an Object containing it's value
// returns a blank Object and an error on fail
func Str2Object(src string) (*Object, error) {
	var (
		// outputs
		tmp   *Object = NewObject()
		blank *Object = NewObject()

		// parsing prep
		ln   int  = len(src)
		char byte = 0

		// parsing state
		isString       bool   = false
		inString       bool   = false
		stringValue    string = ""
		isNumeric      bool   = false
		isNegative     bool   = false
		numericValue   int    = 0
		isFloatlike    bool   = false
		floatlikeValue int    = 0
		floatlikePower int    = 10
	)
	src = strings.TrimSpace(src)
	if src == "true" || src == "false" {
		tmp.SetBool(src == "true")
		return tmp, nil
	}
	// parse loop
	for i := 0; i < ln; i++ {
		// current character
		char = src[i]

		// debug (this actually didnt help cuz everything worked lol)
		//fmt.Printf(
		//	"char: %c\nisString: %t\ninString: %t\nstringValue: %s\nisNumeric: %t\nnumericValue: %d\nisFloatlike: %t\nfloatlikeValue: %d\nfloatlikePower: %d\n",
		//	char, isString, inString, stringValue, isNumeric, numericValue, isFloatlike, floatlikeValue, floatlikePower)

		// parse value
		if char == '"' {
			if !inString {
				isString = true
			}
			inString = !inString
			continue
		} else if char == '-' {
			isNumeric = true
			isNegative = true
			continue
		} else if char == '.' && isNumeric {
			isNumeric = false
			isFloatlike = true
			continue
		} else if !inString && !isNumeric && !isFloatlike {
			// fun fact: to convert an ascii code to a number, subtract 48
			// (or the ascii code of '0')
			var possibleNum int = int(char) - 48
			// debug (useless again)
			//fmt.Println("possibleNum", possibleNum)
			if possibleNum > -1 && possibleNum < 10 {
				isNumeric = true
			} else {
				if !inString {
					return blank, fmt.Errorf(
						"unexpected token '%c'", char)
				}
			}
		}
		if inString {
			stringValue += string(char)
		} else if !inString && stringValue != "" {
			return blank, errors.New("found trailing data after string")
		}
		if isNumeric {
			var num int = int(char) - 48
			if num < 0 || num > 9 {
				return blank, fmt.Errorf(
					"expected digit, but got '%c' instead", char)
			}
			numericValue *= 10
			numericValue += int(char - 48)
		}
		if isFloatlike {
			var num int = int(char) - 48
			if num < 0 || num > 9 {
				return blank, fmt.Errorf(
					"expected digit, but got '%c' instead", char)
			}
			floatlikePower *= 10
			floatlikeValue *= 10
			floatlikeValue += int(char - 48)
		}
	}

	// set value
	if isString {
		tmp.SetString(stringValue)
	}
	if isNumeric {
		if isNegative {
			numericValue = -(numericValue)
		}
		tmp.SetInt(int32(numericValue))
	}
	if isFloatlike {
		var (
			floatlikeInt float32 = float32(numericValue)
			floatlikeDec float32 = float32(floatlikeValue) /
				(float32(floatlikePower) / 10.0)
			floatlikeFin float32 = floatlikeInt + floatlikeDec
		)
		if isNegative {
			floatlikeFin = -(floatlikeFin)
		}
		tmp.SetFloat(floatlikeFin)
	}

	// debug again (but useful this time)
	// fmt.Println(tmp)

	return tmp, nil
}
