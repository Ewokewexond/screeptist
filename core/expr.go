package core

import (
	"errors"
	"strings"
)

type opFunc func(*Object, *Object) (bool, error)

var opMap map[string]opFunc = make(map[string]opFunc)

// IsPossiblyWord returns true if input (possibly) is a word (variable name)
func IsPossiblyWord(src string) bool {
	//fmt.Println("isPossiblyWord ", src)
	src = strings.TrimSpace(src)
	var (
		stringlike bool = src[0] == '"'
		num        int  = int(src[0] - 48)
		numlike    bool = num > -1 && num < 10
		boollike   bool = src == "true" || src == "false"
	)
	//fmt.Println(stringlike, numlike)
	return !(stringlike || numlike || boollike)
}

// AddOperator adds an operator to the global operator map
func AddOperator(op string, f opFunc) {
	opMap[op] = f
}

// ParseExpr parses a raw expression string and returns it's outcome
func ParseExpr(src string, varmap map[string]*Object) (bool, error) {
	var (
		ln int  = len(src)
		c  byte = 0

		// state vars
		hasLeft  bool   = false
		leftRaw  string = ""
		operator opFunc
		rightRaw string = ""
	)
	// parse expression
	for i := 0; i < ln; i++ {
		c = src[i]

		if !hasLeft {
			o, ok := opMap[string(c)]
			if ok {
				hasLeft = true
				operator = o
			} else {
				leftRaw += string(c)
			}
		} else {
			rightRaw += string(c)
		}
	}
	leftRaw = strings.TrimSpace(leftRaw)
	rightRaw = strings.TrimSpace(rightRaw)
	// ensure we have an operator
	if operator == nil {
		return false, errors.New("could not find operator")
	}
	// and a value to compare to
	if rightRaw == "" {
		return false, errors.New("only one side of expression found")
	}
	// get the left value
	var lo *Object
	var loe error
	if IsPossiblyWord(leftRaw) {
		vv, vf := varmap[leftRaw]
		lo = vv
		if !vf {
			loe = errors.New("could not find variable")
		}
	} else {
		lo, loe = Str2Object(leftRaw)
	}
	if loe != nil {
		return false, loe
	}
	// get the right value
	var ro *Object
	var roe error
	if IsPossiblyWord(rightRaw) {
		vv, vf := varmap[rightRaw]
		ro = vv
		if !vf {
			roe = errors.New("could not find variable")
		}
	} else {
		ro, roe = Str2Object(rightRaw)
	}
	if roe != nil {
		return false, roe
	}
	// compare
	or, ore := operator(lo, ro)
	return or, ore
}
