package parser

import (
	"fmt"
	"screeptist/core"
	"strconv"
)

// Argument represents a keyword's argument (word or value)
type Argument struct {
	IsRaw bool
	Raw   string
	Value *core.Object
}

type kwFunc func([]Argument, *VarMap) error
type skwFunc func([]Argument, *VarMap, *Parser) error

var kwMap map[string]kwFunc = make(map[string]kwFunc)
var skwMap map[string]skwFunc = make(map[string]skwFunc)

// AddKw adds a keyword to the global keyword map
func AddKw(kwName string, kw kwFunc) {
	kwMap[kwName] = kw
}

// AddSkw adds a keyword to the global special keyword map
func AddSkw(skwName string, kw skwFunc) {
	skwMap[skwName] = kw
}

// Repr returns a textual representation of an Object
func Repr(obj *core.Object) string {
	var (
		otype  string = obj.TypeName()
		ovalue string
	)
	if obj.IsVoid() {
		ovalue = "(void)"
	}
	if obj.IsString() {
		ovalue = "\"" + obj.StringValue + "\""
	}
	if obj.IsInt() {
		ovalue = strconv.FormatInt(int64(obj.IntValue), 10)
	}
	if obj.IsFloat() {
		ovalue = strconv.FormatFloat(float64(obj.FloatValue), 'f', 3, 32)
	}
	if obj.IsBool() {
		ovalue = strconv.FormatBool(obj.BoolValue)
	}
	return "[" + otype + " " + ovalue + "]"
}

// RunStatement runs a single statement
func RunStatement(kw string, args []Argument, varmap *VarMap, p *Parser) error {
	skwF, isSpecial := skwMap[kw]
	if isSpecial {
		return skwF(args, varmap, p)
	}
	kwF, ok := kwMap[kw]
	if !ok {
		return fmt.Errorf("could not find keyword `%s`", kw)
	}
	return kwF(args, varmap)

}
