package parser

import (
	"fmt"
	"os"
	"screeptist/core"
	"strings"
)

func isPossiblyExpr(src string) bool {
	return strings.HasPrefix(src, "(") && strings.HasSuffix(src, ")")
}

// VarMap is a map of string->Object
type VarMap map[string]*core.Object

// Parser contains a private VarMap and parsing functions
type Parser struct {
	varmap VarMap
}

// NewParser returns a new *Parser with an empty varmap
func NewParser() *Parser {
	var tmp *Parser = new(Parser)
	tmp.Flush()
	return tmp
}

// Flush resets the parser's varmap to be empty
func (p *Parser) Flush() {
	p.varmap = make(VarMap)
}

// Parse executes a single statement
func (p *Parser) Parse(src string) error {
	var (
		ln int  = len(src)
		c  byte = 0

		// parsing status
		tokenList []string
		tokenLast string = ""
		inString  bool   = false
		inExpr    bool   = false
	)
	// get the tokens
	for i := 0; i < ln; i++ {
		c = src[i]

		switch c {
		case '"':
			if inExpr {
				break
			}
			if !inString && tokenLast != "" {
				tokenList = append(tokenList, tokenLast)
				tokenLast = ""
			}
			tokenLast += string(c)
			inString = !inString
			// we have to check if the token is longer than 1, to ensure we
			// we have at least 2 chars (the two quotes)
			if !inString && len(tokenLast) > 1 {
				tokenList = append(tokenList, tokenLast)
				tokenLast = ""
			}
			break
		case '(':
			if !inExpr {
				inExpr = true
				tokenLast += string(c)
			}
			break
		case ')':
			if inExpr {
				inExpr = false
				tokenLast += string(c)
				tokenList = append(tokenList, tokenLast)
				tokenLast = ""
			}
			break
		case ' ':

			if !inString && !inExpr {
				tokenList = append(tokenList, tokenLast)
				tokenLast = ""
				break
			}
			tokenLast += string(c)
			break
		default:
			tokenLast += string(c)
			break
		}
	}
	tokenList = append(tokenList, tokenLast)

	//fmt.Println("tokenList be like: ", tokenList)

	// turn string tokens into a keyword and arguments
	var (
		tmp  string
		tmpO *core.Object
		tmpA Argument

		kw      string   = tokenList[0]
		argsRaw []string = tokenList[1:]
		args    []Argument
	)
	for i := 0; i < len(argsRaw); i++ {
		tmp = strings.TrimSpace(argsRaw[i])
		if tmp == "" {
			continue
		}
		if core.IsPossiblyWord(tmp) && !isPossiblyExpr(tmp) {
			tmpA = Argument{true, tmp, nil}
		} else if isPossiblyExpr(tmp) {
			tmp = tmp[1 : len(tmp)-1]
			//fmt.Println("tmp be like: ", tmp)
			tmpO = core.NewObject()
			res, err := core.ParseExpr(tmp, p.varmap)
			if err != nil {
				return err
			}
			tmpO.SetBool(res)
			tmpA = Argument{false, tmp, tmpO}
		} else {
			res, err := core.Str2Object(tmp)
			if err != nil {
				return err
			}
			tmpA = Argument{false, tmp, res}
		}
		args = append(args, tmpA)
	}

	return RunStatement(kw, args, &p.varmap, p)
}

// RunFile opens, reads and executes a file
func (p *Parser) RunFile(filename string) error {
	file, oerr := os.Open(filename)
	if oerr != nil {
		return fmt.Errorf("could not open file: %v", oerr)
	}
	buff := make([]byte, 0xFFFF)
	_, rerr := file.Read(buff)
	if rerr != nil {
		return fmt.Errorf("could not read file: %v", rerr)
	}
	code := string(buff)
	for _, line := range strings.Split(code, "\n") {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "//") {
			continue
		}
		err := p.Parse(strings.TrimSpace(line))
		if err != nil {
			return err
		}
	}
	return nil
}
