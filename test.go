package main

import (
	"fmt"
	"os"
	"screeptist/core"
	"screeptist/generic"
)

func main() {
	fmt.Println("screeptist ", core.VERSION)
	p := generic.GetParser()
	if len(os.Args) < 2 {
		fmt.Println("no file specified")
	} else {
		err := p.RunFile(os.Args[1])
		if err != nil {
			fmt.Println(err)
		}
	}
}
