# screeptist
A side project: shell-like scripting language in Go


# Currently working
* Dynamically typed objects
* Parsing (string -> tokens -> arguments)
* Keywords (`func([]Argument, *VarMap) error`)
* `LOAD`ing files (imports in spirit)

## Working keywords
* `SET <word> <value>`
* `SHOW <value> <value> ...`
* `VOID <word>`
* `LOAD "<filename>"`
* `PRINT <value>` (if value isn't a string, functionally the same as `SHOW`)
* `ADD <word> <value>`
* `SUB <word> <value>`
* `MUL <word> <value>`
* `DIV <word> <value>`
* `MOD <word> <value>`
* `CONCAT <word> <value>`
* `NAV "<folder>"`
* `GETENV <word> "<variable>"`

# Wasn't working but is fixed

## Expressions
There were two problems: one small and one big. The small one was that the parser didn't ignore empty tokens, and was an easy fix. The bigger one caused me a headache. The parser was broken, and didn't tokenize the expression literal correctly. That caused a runtime error because the return value of the expression parser was `nil` and therefore the variable that should have contained the result was set to `nil`. This caused a problem with the `SHOW` keyword, which calls the `parser.Repr` function, which calls the `(*Object).TypeName` function on a `nil`. This is kind of because there's no error handling at the moment.

## String literals
This had the same small issue as expressions, the parser didn't ignore empty tokens, causing empty strings to be used as keyword arguments.

## Parser ignores spaces?!?!
I forgot Go's cases aren't fallthrough, so he parser never appended spaces to tokens, therefore expression and string tokens didn't have them at all.

# Plans

## Compiling
Automatically generating a `.go` file with `package main`, containing a string which would be the script and then simply running a generic interpreter, as you would with a file.

## ~~Arithmetic~~
~~Assembler-inspired arithmetic keywords, which would modify variable values in-place.~~

Added in 0.0.4

## Managing the local environment
Allowing the user to manage folders and files in a local context, which would be simply a file path, stored in the `!PATH` variable as a string.

Planned for ~0.0.5

## Executables
Preferably with a `RUN` keyword, which would accept 1 string argument and execute the string, as you would in a command prompt.